// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h" 
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = -60.f;
	ElementsNumber = 4;
	LastMoveDirection = EMovementDirection::UP;
	MovementSpeed = 0.5f;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	bIsElemOffsetMoment = true;
	SetActorTickInterval(MovementSpeed);
	AddSnakeElementOnBeginPlay(ElementsNumber);
} 

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}
 

void ASnakeBase::AddSnakeElementOnBeginPlay(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		//FVector LocationOfLastElement(SnakeElements[SnakeElements.Num() - 1]->GetActorLocation());
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 60);
		FTransform NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		int ElemIndex = SnakeElements.Add(NewSnakeElement);

		if (ElemIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();

		}
	}
}

void ASnakeBase::AddSnakeElementOnGetFood()
{
		FVector LocationOfLastElement(SnakeElements[SnakeElements.Num() - 1]->GetActorLocation()); 
		FTransform NewTransform = FTransform(LocationOfLastElement);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;
		SnakeElements.Add(NewSnakeElement);
}

void ASnakeBase::RemoveSnakeElements(int ElementsNum)
{
	for (size_t i = 0; i < SnakeElements.Num(); i++)
	{
		SnakeElements[i]->Destroy();
	}
}

 


void ASnakeBase::Move()
{
	FVector MovementVector(FVector::ZeroVector);
	float MovementSpeedDelta = - ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedDelta; 
		break;

	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeedDelta; 
		break;

	case EMovementDirection::RIGHT:
		MovementVector.Y += MovementSpeedDelta; 
		break;

	case EMovementDirection::LEFT:
		MovementVector.Y -= MovementSpeedDelta; 
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; --i)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	bIsElemOffsetMoment = true;

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);

		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

